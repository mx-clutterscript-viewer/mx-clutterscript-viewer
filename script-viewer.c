
#include <clutter/clutter.h>
#include <mx/mx.h>

static void
stage_removed_cb (ClutterStageManager *manager,
                  ClutterStage        *stage)
{
  GSList *stages = clutter_stage_manager_list_stages (manager);

  if (!stages)
    clutter_main_quit ();
  else
    g_slist_free (stages);
}

int
main (int argc, char **argv)
{
  gint i;
  GList *objects;
  ClutterScript *script;

  GError *error = NULL;
  gboolean stage_found = FALSE;
  gboolean script_found = FALSE;

  if (argc < 2)
    {
      g_message ("File not specified");
      return 1;
    }

  if (!g_file_test (argv[1], G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR))
    {
      g_message ("Not a file, or file not found");
      return 2;
    }

  clutter_init (&argc, &argv);

  script = clutter_script_new ();

  for (i = 1; i < argc; i++)
    {
      if (g_str_has_suffix (argv[i], ".css"))
        {
          if (!mx_style_load_from_file (mx_style_get_default (),
                                        argv[i], &error))
            {
              g_message ("Error loading style: %s", error->message);
              g_error_free (error);
            }
        }
      else if (clutter_script_load_from_file (script, argv[i], &error) == 0)
        {
          g_message ("Error loading script: %s", error->message);
          g_error_free (error);
        }
      else
        script_found = TRUE;
    }

  if (!script_found)
    {
      g_message ("No script loaded");
      return 3;
    }

  for (objects = clutter_script_list_objects (script);
       objects; objects = g_list_delete_link (objects, objects))
    {
      GObject *object = objects->data;

      if (CLUTTER_IS_STAGE (object))
        {
          clutter_actor_show (CLUTTER_ACTOR (object));
          stage_found = TRUE;
        }
      else if (CLUTTER_IS_ACTOR (object) &&
               !clutter_actor_get_parent (CLUTTER_ACTOR (object)))
        {
          ClutterActor *stage = clutter_stage_new ();

          clutter_stage_set_user_resizable (CLUTTER_STAGE (stage), TRUE);

          clutter_container_add_actor (CLUTTER_CONTAINER (stage),
                                       CLUTTER_ACTOR (object));

          clutter_actor_show (stage);
          stage_found = TRUE;
        }
      else if (MX_IS_WINDOW (object))
        {
          mx_window_show (MX_WINDOW (object));
          stage_found = TRUE;
        }
    }

  if (stage_found)
    {
      ClutterStageManager *manager = clutter_stage_manager_get_default ();

      g_signal_connect (manager, "stage-removed",
                        G_CALLBACK (stage_removed_cb), NULL);

      clutter_main ();
    }
  else
    g_message ("Script contains no ClutterStage instances "
               "or unparented actors");

  return 0;
}
