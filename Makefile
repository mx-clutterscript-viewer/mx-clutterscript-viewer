
all: script-viewer

clean:
	rm -f script-viewer

script-viewer: script-viewer.c
	$(CC) -o script-viewer script-viewer.c -Wall -O0 -g `pkg-config --cflags --libs clutter-1.0 mx-1.0`

